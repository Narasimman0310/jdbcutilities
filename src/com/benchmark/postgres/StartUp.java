package com.benchmark.postgres;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class StartUp {
	static Properties prop=new Properties();
	public static void main(String[] args) throws InterruptedException, IOException {
		
		InputStream striimProp = new FileInputStream(args[0]);
		prop.load(striimProp);
		for (String key: prop.stringPropertyNames()) {
			System.out.println(key + ": " + prop.getProperty(key));
		}
		Thread insertThread1=new PostgresInsertPart1();
		Thread insertThread2=new PostgresInsertPart2();
		insertThread1.setName("Part1InsertThread");
		insertThread2.setName("Part2InsertThread");
		insertThread1.start();
		System.out.println("The thread "+insertThread1.getName()+" is running.");
		insertThread2.start();
		System.out.println("The thread "+insertThread2.getName()+" is running.");
	}

}
