package com.benchmark.postgres;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;

public class PostgresUpdateUtil {
	
	String table_name;
	String colId;
	public PostgresUpdateUtil(String tbl_name ,String id) {
		this.table_name=tbl_name;
		this.colId=id;
	}
	public int colInt;
	public long longCol;
	public int smallIntCol;
	public String stringCol;
	public String textCol;
	public Date dateCol;
	public Time timeCol;
	public Timestamp timeStampCol;
	
	int randColumn;
	
	SimpleDateFormat timeStampFormatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");  
	SimpleDateFormat timeFormatter=new SimpleDateFormat("HH:mm:ss");
	SimpleDateFormat dateFormatter=new SimpleDateFormat("dd-MM-yyyy");
	
	java.util.Date date = new java.util.Date();
	
	public void updateCheck() {
//		System.out.println("start Time: "+java.time.LocalTime.now());
		String url = "jdbc:postgresql://localhost:5432/postgres";
        String user = "postgres";
        String password = "postgres";
        int totRow=100;
        String[] updateCols= {"col_9","col_10","col_12"};
        
        try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        Connection con = null;
		try {
			con = DriverManager.getConnection(url, user, password);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        try {
			con.setAutoCommit(false);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        PreparedStatement pst = null ;

        try {
        	
        	String query=null;
//        	update table_1 set col_4='Demo' where id_1 =(SELECT id_1
//        	FROM table_1 OFFSET floor(random() * (
//        	SELECT COUNT(*) FROM table_1)) LIMIT 1);
        	
        	System.out.println("Executing the Query:"+"Running update Query");
        	System.out.println("Sample Query:"+ "\nupdate " +table_name+ " set col_4=? where " +colId+ "=(SELECT \n"+colId+ "\nFROM        \n"+table_name+ " OFFSET floor(random() * (\nSELECT COUNT(*) \n FROM table_1)) LIMIT 1);");
        	
        	StringBuilder sb=null;
        	int updateCount=0;
        	String colForUpdate;
            for(int rowCount=0;rowCount<totRow;rowCount++)
            {
            	sb=new StringBuilder();
            	randColumn =new Random().nextInt(updateCols.length);
            	colForUpdate=updateCols[randColumn];
//            	System.out.println("Random col to upadate:"+colForUpdate);
            	sb.append("UPDATE "+table_name+" SET "+colForUpdate);
            	sb.append("=? ");
            	sb.append(" WHERE ");
            	sb.append(colId + "=");
            	sb.append("(SELECT ");
            	sb.append(colId+ " FROM " +table_name+ " OFFSET floor(random() * (");
            	sb.append("\n");
            	sb.append("SELECT COUNT(*) FROM ");
            	sb.append(table_name);
            	sb.append(")) LIMIT 1 )");
            	
//            	System.out.println(sb.toString());
            	query=sb.toString();
            	pst = con.prepareStatement(query);
        		
        		if(colForUpdate.equalsIgnoreCase("col_9")) {
        			pst.setInt(1, generateInt());
        		}
        		else if(colForUpdate.equalsIgnoreCase("col_10")) {
        			pst.setLong(1, generateLong());
        		}
        		else if(colForUpdate.equalsIgnoreCase("col_12")) {
        			pst.setString(1, generateString());
        		}
        		
        		pst.executeUpdate();
        		con.commit();
        	//	pst.executeUpdate();
         	//	con.commit();
        		
        		//pst.addBatch();
//        		if(rowCount%1==0)
//        		{
//        			//pst.executeBatch();
//        			//pst.clearBatch();
//            		
//        		}
//        		System.out.println("Prepared Statement after bind variables set:\n\t" + pst.toString());
        		updateCount++;
        		
            }
            
           // pst.executeBatch();
            System.out.println("Updated rows:"+updateCount);
//            System.out.println("End Time: "+java.time.LocalTime.now());

        } catch (SQLException ex) {
        	ex.printStackTrace();
        }
			try {
				if (pst != null)
					pst.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (con != null)
					con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

	}
	
	public int generateInt()
	{
		colInt=getRandomNumberInRange(1, 99);
		return colInt;
	}
	
	public long generateLong()
	{
		longCol=getRandomNumberInRange(10000, 150000);
		return longCol;
	}
	
	public String generateString()
	{
		stringCol=RandomStringUtils.randomAlphabetic(10);
		return stringCol;
	}
	
	public String generateText()
	{
		textCol=RandomStringUtils.randomAlphabetic(20);
		return textCol;
	}
	
	public Date generateDate() throws ParseException
	{
		String tempDate=dateFormatter.format(date);
		java.util.Date parsedate = dateFormatter.parse(tempDate);
		dateCol = new java.sql.Date(parsedate.getTime());
		return dateCol;
	}
	public Timestamp generateTimestmap() throws ParseException
	{
		String tempTimeStamp=timeStampFormatter.format(date);
		timeStampCol=new Timestamp(timeStampFormatter.parse(tempTimeStamp).getTime());
		return timeStampCol;
	}
	
	public Time genreateTimeCol() throws ParseException
	{
		String tempTime=timeFormatter.format(date);
//		timeCol = (Time)timeFormatter.parse(tempTime);
		timeCol=new Time(timeFormatter.parse(tempTime).getTime());
		return timeCol;
	}
	public int genrateSmallIntCol()
	{
		smallIntCol=getRandomNumberInRange(100, 999);
		return smallIntCol;
	}
	
	private int getRandomNumberInRange(int min, int max) {

		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		return (int) (Math.random() * ((max - min) + 1)) + min;
	}
	public static void main(String[] args) {
		System.out.println("start Time: "+java.time.LocalTime.now()); 
		PostgresUpdateUtil testObj =new PostgresUpdateUtil("table_5","id_5");
//		System.out.println(testObj.generateQuery("table"));
		testObj.updateCheck();
		System.out.println("End Time: "+java.time.LocalTime.now()); 
	}
}
