package com.indium.Mssql;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class TineStampDemo {

	public static void main(String[] args) throws ParseException {
//		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
	    Date date = sdf.parse("2013-01-09T19:32:49.103+05:30"); 
	    sdf.setTimeZone(TimeZone.getTimeZone("IST"));
	    System.out.println("Formated Output:"+sdf.format(date));
	}
}
