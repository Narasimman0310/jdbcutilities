package com.indium.Mssql;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;



public class Mysql_Updates {
	
	static Properties prop = new Properties();
	public static void main(String[] args) throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		InputStream striimProp = new FileInputStream("/conf/striim-cdc.properties");
		prop.load(striimProp);
		int updateThreadCount=Integer.parseInt(prop.getProperty("deleteThreads"));
		for (int i = 0; i < updateThreadCount; i++) {
			Thread object = new Thread(new UpdateThread());
			object.start();
			Thread.sleep(5000);
		}

	}

}

class Msql_Udate_Threads implements Runnable{
	static Connection conn = null;
	static String dbURL = null;
//	static String user = null;
//	static String pass = null;
	static CallableStatement callableStatement = null;
	static String sql = null;
	static String startTime;
	static String endTime;
	Properties prop=new Properties();
	
	@Override
	public void run() {

		try {
			prop=Mysql_Updates.prop;
			dbURL = prop.getProperty("dbURL");
			conn = DriverManager.getConnection(dbURL);

			if (conn != null) {
				System.out.println("Current Thread:"+Thread.currentThread().getName());
				startTime = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
				System.out.println("start time is : " + startTime);
				sql = "{call " + prop.getProperty("updateSP_name") +"(?)}";
				callableStatement = conn.prepareCall(sql);
				callableStatement.setInt(1, Integer.parseInt(prop.getProperty("tableCount")));
//				callableStatement.setInt(2, Integer.parseInt(prop.getProperty("colCount")));
//				callableStatement.setInt(3, Integer.parseInt(prop.getProperty("tableCount")));
				callableStatement.executeUpdate();
				callableStatement.close();
				conn.close();
				endTime = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
				System.out.println("end time is : " + endTime);
				System.out.println(prop.getProperty("updateSP_name")+" SP executed successfully and check data in DB...!");
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
	}
}