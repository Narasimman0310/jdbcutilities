package com.indium.Mssql;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

public class RunInsertCDC {
	static Properties prop = new Properties();

	public static void main(String[] args) throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		InputStream striimProp = new FileInputStream(args[0]);
		prop.load(striimProp);
		int insertThreadCount = Integer.parseInt(prop.getProperty("insertThreads"));
		System.out.println("Thread Count:"+insertThreadCount);
		
		
		Thread[] threadList=new Thread[insertThreadCount+1];
		
		for (int i = 1; i <= insertThreadCount; i++) {
			threadList[i] = new InsertThread(sp_table_name);
			threadList[i].start();
//			System.out.println("iteration:"+i);
//			String sp_table_name=prop.getProperty("table_name");
//			
//			if(insertThreadCount==10)
//			{
//				sp_table_name=sp_table_name+(i);
//				System.out.println("Table iteration:"+sp_table_name);
//				threadList[i] = new InsertThread(sp_table_name);
//				threadList[i].start();
//			}
//			
//			else if(insertThreadCount==20){
//				if(i%2==0)
//				{
//				sp_table_name=sp_table_name+(i);
//				System.out.println("Table iteration:"+sp_table_name);
//				}
//				else {
//					sp_table_name=sp_table_name+(i+1);
//					System.out.println("Table iteration:"+sp_table_name);
//				}
//				threadList[i] = new InsertThread(sp_table_name);
//				
//				threadList[i].start();
//			}
			
//			Thread object = new Thread(new InsertThread());
//			object.start();
//			Thread.sleep(5000);
		}
//		for (int i = 0; i < insertThreadCount; i++) {
//			threadList[i].join();
//		}

	}
}
class InsertThread extends Thread {
	
	String table_name=null;
	Connection conn = null;
	String dbURL = null;
	String user = null;
	String pass = null;
	CallableStatement callableStatement = null;
	String sql = null;
	String startTime;
	String endTime;
	Properties prop=new Properties();
	
	public InsertThread(String tbl_name)
	{
		this.table_name=tbl_name;
	}
	
	@Override
	public void run() {
		try {
			prop=RunInsertCDC.prop;
			dbURL = prop.getProperty("dbURL");
//			System.out.println(prop.getProperty("dbURL"));
			conn = DriverManager.getConnection(dbURL);

			if (conn != null) {
				startTime = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
				System.out.println("start time is : " + startTime);
//				sql = "{call " + prop.getProperty("insertSP_name") +"(?,?,?)}";
				sql = "{call " + prop.getProperty("insertSP_name") +"(?,?,?,?)}";
				callableStatement = conn.prepareCall(sql);
				callableStatement.setInt(1, Integer.parseInt(prop.getProperty("tableCount")));
				callableStatement.setInt(2, Integer.parseInt(prop.getProperty("colCount")));
				callableStatement.setInt(3, Integer.parseInt(prop.getProperty("rowCount")));
				callableStatement.setString(4, prop.getProperty(table_name));
				callableStatement.executeUpdate();
				callableStatement.close();
//				conn.close();
				endTime = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
				System.out.println("end time is : " + endTime);
				System.out.println(prop.getProperty("insertSP_name") +" SP executed successfully and check data in DB...!");
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
//			if (callableStatement != null) {
//	            try {
//	            	callableStatement.close();
//	            } catch (SQLException ex) {
//	            	ex.printStackTrace();
//	            }
//		}
	}

}
}

