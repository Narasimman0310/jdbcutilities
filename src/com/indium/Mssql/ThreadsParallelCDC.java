package com.indium.Mssql;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

public class ThreadsParallelCDC {
	static Properties striimProp = new Properties();
	public static void main(String[] args) throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		InputStream propFile = new FileInputStream("/home/indium/Indium/StriimIntialLoad/Striim-Prop/striim-cdc.properties");
		striimProp.load(propFile);
		Thread insertThread=new Thread(new InsertionThread());
		Thread deleteThread=new Thread(new DeletionThread());
		Thread updateThread=new Thread(new UpdationThread());
		insertThread.start();
		Thread.sleep(500);
		deleteThread.start();
		Thread.sleep(500);
		updateThread.start();
	}
}
class InsertionThread extends Thread {
	
	String table_name=null;
	Connection conn = null;
	String dbURL = null;
	String user = null;
	String pass = null;
	CallableStatement callableStatement = null;
	String sql = null;
	String startTime;
	String endTime;
	Properties prop=new Properties();
	
	public void InsertThread(String tbl_name)
	{
		this.table_name=tbl_name;
	}
	
	@Override
	public void run() {
		try {
			prop=ThreadsParallelCDC.striimProp;
			dbURL = prop.getProperty("dbURL");
//			System.out.println(prop.getProperty("dbURL"));
			conn = DriverManager.getConnection(dbURL);

			if (conn != null) {
				startTime = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
				System.out.println("Insert thread "+Thread.currentThread().getName()+" started");
				System.out.println("start time is : " + startTime);
//				sql = "{call " + prop.getProperty("insertSP_name") +"(?,?,?)}";
				sql = "{call " + prop.getProperty("insertSP_name") +"(?,?,?,?)}";
				callableStatement = conn.prepareCall(sql);
				callableStatement.setInt(1, Integer.parseInt(prop.getProperty("tableCount")));
				callableStatement.setInt(2, Integer.parseInt(prop.getProperty("colCount")));
				callableStatement.setInt(3, Integer.parseInt(prop.getProperty("rowCount")));
				callableStatement.setString(4, prop.getProperty(table_name));
				callableStatement.executeUpdate();
				callableStatement.close();
//				conn.close();
				endTime = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
				System.out.println("end time is : " + endTime);
				System.out.println(prop.getProperty("insertSP_name") +" SP executed successfully and check data in DB...!");
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
	}
}
}

class DeletionThread implements Runnable {
	static Connection conn = null;
	static String dbURL = null;
	static String user = null;
	static String pass = null;
	static CallableStatement callableStatement = null;
	static String sql = null;
	static String startTime;
	static String endTime;
	Properties prop=new Properties();
	
	@Override
	public void run() {

		try {
			prop=ThreadsParallelCDC.striimProp;
			dbURL = prop.getProperty("dbURL");
			conn = DriverManager.getConnection(dbURL);

			if (conn != null) {
				System.out.println("Delete thread "+Thread.currentThread().getName()+" started");
				startTime = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
				System.out.println("start time is : " + startTime);
				sql = "{call " + prop.getProperty("deleteSP_name") +"(?)}";
				callableStatement = conn.prepareCall(sql);
				callableStatement.setInt(1, Integer.parseInt(prop.getProperty("tableCount")));
//				callableStatement.setInt(2, Integer.parseInt(prop.getProperty("colCount")));
//				callableStatement.setInt(3, Integer.parseInt(prop.getProperty("tableCount")));
//				callableStatement.executeUpdate();
				callableStatement.close();
				conn.close();
				endTime = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
				System.out.println("end time is : " + endTime);
				System.out.println(prop.getProperty("deleteSP_name")+" SP executed successfully and check data in DB...!");
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
	}

}
class UpdationThread implements Runnable {
	static Connection conn = null;
	static String dbURL = null;
	static String user = null;
	static String pass = null;
	static CallableStatement callableStatement = null;
	static String sql = null;
	static String startTime;
	static String endTime;
	Properties prop=new Properties();
	
	@Override
	public void run() {

		try {
			prop=ThreadsParallelCDC.striimProp;
			dbURL = prop.getProperty("dbURL");
			conn = DriverManager.getConnection(dbURL);

			if (conn != null) {
				System.out.println("Update thread "+Thread.currentThread().getName()+" started");
				startTime = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
				System.out.println("start time is : " + startTime);
				sql = "{call " + prop.getProperty("updateSP_name") +"(?)}";
				callableStatement = conn.prepareCall(sql);
				callableStatement.setInt(1, Integer.parseInt(prop.getProperty("tableCount")));
//				callableStatement.setInt(2, Integer.parseInt(prop.getProperty("colCount")));
//				callableStatement.setInt(3, Integer.parseInt(prop.getProperty("tableCount")));
				callableStatement.executeUpdate();
				callableStatement.close();
				conn.close();
				endTime = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
				System.out.println("end time is : " + endTime);
				System.out.println(prop.getProperty("updateSP_name")+" SP executed successfully and check data in DB...!");
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
	}
}