package com.indium.Mssql;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class JdbcSQLServerConnection {

	// https://www.journaldev.com/2502/callablestatement-in-java-example

	static Connection conn = null;
	static String dbURL = null;
	static String user = null;
	static String pass = null;
	static CallableStatement callableStatement = null;
	static String sql = null;

	static String startTime;
	static String endTime;

	public static void main(String[] args) {

		spCallRecordInsert();
		// spCallTableCreate();
	}

	public static void spCallRecordInsert() {

		// Connection conn = null;
		// CallableStatement callableStatement = null;
		// String sql = null;

		try {

//			dbURL = "jdbc:sqlserver://10.10.112.134\\GURU1:1433;DatabaseName=test";
			dbURL = "jdbc:sqlserver://indium.database.windows.net:1433;database=indium_test;user=indium@indium;password=Striim@123;encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30";	

				
			// user = "new2";
			// pass = "P@ssw0rd";
			conn = DriverManager.getConnection(dbURL);
			System.err.println("connected successfullt....!!");

			if (conn != null) {
				startTime = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
				System.out.println("start time is : " + startTime);
				// sql = "{call sp_ct}";

				sql = "{call sp_load_data (?,?)}";
				// @tb_cnt =100, rw_cnt=100000;
				callableStatement = conn.prepareCall(sql);
				callableStatement.setInt(1, 200);
				callableStatement.setInt(2, 100000);
				callableStatement.execute();
				callableStatement.close();
				conn.close();
				endTime = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
				System.out.println("end time is : " + endTime);
				System.out.println("SP executed successfully and check data in DB...!");
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
	}

	public static void spCallTableCreate() {

		// Connection conn = null;
		// CallableStatement callableStatement = null;
		// String sql = null;

		try {

			dbURL = "jdbc:sqlserver://10.10.112.134\\GURU1:1433;DatabaseName=test";
			user = "indium";
			pass = "Striim@123";
			conn = DriverManager.getConnection(dbURL, user, pass);

			if (conn != null) {

				sql = "{call sp_ct}";
				callableStatement = conn.prepareCall(sql);
				callableStatement.executeUpdate();
				System.out.println("SP executed successfully and check table in DB...!");
				callableStatement.close();
				conn.close();
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
	}

}