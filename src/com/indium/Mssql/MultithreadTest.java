package com.indium.Mssql;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class MultithreadingDemo implements Runnable 
{ 
    public void run() 
    { 
        try
        { 
            // Displaying the thread that is running 
            System.out.println ("Thread " + 
                                Thread.currentThread().getId() + 
                                " is running"); 
  
        } 
        catch (Exception e) 
        { 
            // Throwing an exception 
            System.out.println ("Exception is caught"); 
        } 
    } 
} 
  
// Main Class 
class MultithreadTest
{ 
    public static void main(String[] args) 
    {
        ExecutorService executor = Executors.newCachedThreadPool();
//        ExecutorService executor = Executors.newFixedThreadPool(10);

        int n = 10; // Number of threads 
        for (int i=0; i<n; i++) 
        { 
//            Thread object = new Thread(new MultithreadingDemo()); 
//            System.out.println("Current Thread:"+i);
//            object.start(); 
        	 Runnable worker = new MultithreadingDemo();
             executor.execute(worker);
        } 
        executor.shutdown();
        while (!executor.isTerminated()) {
        }
        System.out.println("Finished all threads");
    }
} 
