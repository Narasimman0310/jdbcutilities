package com.indium.Mssql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;

public class MssqlConnection {
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
////		System.setProperty("sun.security.krb5.debug", "true");
//		System.setProperty("java.security.krb5.kdc", "52.170.19.105");
//		
//		System.setProperty("java.security.krb5.realm","STRIIM.IO"); 
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		
//		Class.forName("net.sourceforge.jtds.jdbc.Driver");
//		String jdbcURL = "jdbc:sqlserver://indium.database.windows.net:1433;database=indium_test;user=indium@indium;password=Striim@123;encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30";
//		String jdbcURL = "jdbc:sqlserver://35.229.92.153:1433;database=striim;domain=STRIIM.IO;integratedSecurity=true;authenticationScheme=JavaKerberos;userName=indium@STRIIM.IO;password=Windows@12345";
		
//		String jdbcURL ="jdbc:sqlserver://35.229.92.153:1433;database=striim;domain=STRIIM.IO;integratedSecurity=true;";
		
//		String jdbcURL="jdbc:jtds:sqlserver://35.229.92.153:1433/striim;domain=STRIIM.IO;useNTLMv2=true;integratedSecurity=false;";
//		jdbc:jtds:sqlserver://server-name/database_name;instance=instance_name

		SQLServerDataSource ds = new SQLServerDataSource();

//		String jdbcURL = "jdbc:sqlserver://35.237.169.206:1433;domain=STRIIM.IO;authenticationScheme=NTLM;user=indium;password=Windows@12345;";
		
		String jdbcURL = "jdbc:sqlserver://35.237.169.206:1433;authenticationScheme=JavaKerberos";
//		String jdbcURL = "jdbc:sqlserver://35.237.169.206:1433;integratedSecurity=false";
		
//		userName=indium@STRIIM.IO;password=Windows@12345
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(jdbcURL,"indium@striim.io","Windows@12345");
			System.out.println("Connected..");
			String sql="select * from dbo.test";
			Statement statement = connection.createStatement();
			ResultSet rs=statement.executeQuery(sql);
			while (rs.next()) {
                System.out.println(rs.getString(1)+","+rs.getString(2));
            }
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connection.close();
		}
		
	}

}
