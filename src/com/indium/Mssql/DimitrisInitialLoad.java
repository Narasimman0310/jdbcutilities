package com.indium.Mssql;

import java.io.FileInputStream;

import java.io.IOException;
import java.io.InputStream;
import java.sql.CallableStatement;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;


public class DimitrisInitialLoad {
	static Properties prop = new Properties();
	public static void main(String[] args) throws InterruptedException, IOException {
		
		InputStream striimProp = new FileInputStream("conf\\striim-cdc.properties.txt");
		prop.load(striimProp);
		System.out.println(prop);
		System.out.println(prop.getProperty("dbURL"));;

		int insertThreadCount = Integer.parseInt(prop.getProperty("insertThreads")); // umber of threads   //para1
		int deleteThreadCount=Integer.parseInt(prop.getProperty("updateThreads"));
		int updateThreadCount=Integer.parseInt(prop.getProperty("deleteThreads"));
//		for (int i = 0; i < insertThreadCount; i++) {
//			Thread object = new Thread(new InsertThread());
//			object.start();
//			Thread.sleep(5000);
//		}
//		for (int i = 0; i < deleteThreadCount; i++) {
//			Thread object = new Thread(new DeleteThread());
//			object.start();
//			Thread.sleep(5000);
//		}
//		for (int i = 0; i < updateThreadCount; i++) {
//			Thread object = new Thread(new UpdateThread());
//			object.start();
//			Thread.sleep(5000);
//		}
//		
	}
}

//class InsertThread implements Runnable {
//	static Connection conn = null;
//	static String dbURL = null;
//	static String user = null;
//	static String pass = null;
//	static CallableStatement callableStatement = null;
//	static String sql = null;
//	static String startTime;
//	static String endTime;
//	Properties prop=new Properties();
//	
//	@Override
//	public void run() {
//
//		try {
//			prop=DimitrisInitialLoad.prop;
//			dbURL = prop.getProperty("dbURL");
//			conn = DriverManager.getConnection(dbURL);
//
//			if (conn != null) {
//				startTime = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
//				System.out.println("start time is : " + startTime);
//				sql = "{call " + prop.getProperty("insertSP_name") +"(?,?,?)}";
//				callableStatement = conn.prepareCall(sql);
//				callableStatement.setInt(1, Integer.parseInt(prop.getProperty("tableCount")));
//				callableStatement.setInt(2, Integer.parseInt(prop.getProperty("colCount")));
//				callableStatement.setInt(3, Integer.parseInt(prop.getProperty("tableCount")));
//				callableStatement.executeUpdate();
//				callableStatement.close();
//				conn.close();
//				endTime = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
//				System.out.println("end time is : " + endTime);
//				System.out.println(prop.getProperty("insertSP_name") +" SP executed successfully and check data in DB...!");
//			}
//		} catch (SQLException ex) {
//			ex.printStackTrace();
//		} finally {
//			try {
//				if (conn != null && !conn.isClosed()) {
//					conn.close();
//				}
//			} catch (SQLException ex) {
//				ex.printStackTrace();
//			}
//		}
//	}
//
//}

//class DeleteThread implements Runnable {
//
//	static Connection conn = null;
//	static String dbURL = null;
//	static String user = null;
//	static String pass = null;
//	static CallableStatement callableStatement = null;
//	static String sql = null;
//	static String startTime;
//	static String endTime;
//	Properties prop=new Properties();
//	
//	@Override
//	public void run() {
//
//		try {
//			prop=DimitrisInitialLoad.prop;
//			dbURL = prop.getProperty("dbURL");
//			conn = DriverManager.getConnection(dbURL);
//
//			if (conn != null) {
//				startTime = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
//				System.out.println("start time is : " + startTime);
//				sql = "{call " + prop.getProperty("deleteSP_name") +"(?)}";
//				callableStatement = conn.prepareCall(sql);
//				callableStatement.setInt(1, Integer.parseInt(prop.getProperty("tableCount")));
////				callableStatement.setInt(2, Integer.parseInt(prop.getProperty("colCount")));
////				callableStatement.setInt(3, Integer.parseInt(prop.getProperty("tableCount")));
////				callableStatement.executeUpdate();
//				callableStatement.close();
//				conn.close();
//				endTime = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
//				System.out.println("end time is : " + endTime);
//				System.out.println(prop.getProperty("deleteSP_name")+" SP executed successfully and check data in DB...!");
//			}
//		} catch (SQLException ex) {
//			ex.printStackTrace();
//		} finally {
//			try {
//				if (conn != null && !conn.isClosed()) {
//					conn.close();
//				}
//			} catch (SQLException ex) {
//				ex.printStackTrace();
//			}
//		}
//	}
//
//}

//class UpdateThread implements Runnable {
//	static Connection conn = null;
//	static String dbURL = null;
//	static String user = null;
//	static String pass = null;
//	static CallableStatement callableStatement = null;
//	static String sql = null;
//	static String startTime;
//	static String endTime;
//	Properties prop=new Properties();
//	
//	@Override
//	public void run() {
//
//		try {
//			prop=DimitrisInitialLoad.prop;
//			dbURL = prop.getProperty("dbURL");
//			conn = DriverManager.getConnection(dbURL);
//
//			if (conn != null) {
//				startTime = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
//				System.out.println("start time is : " + startTime);
//				sql = "{call " + prop.getProperty("updateSP_name") +"(?)}";
//				callableStatement = conn.prepareCall(sql);
//				callableStatement.setInt(1, Integer.parseInt(prop.getProperty("tableCount")));
////				callableStatement.setInt(2, Integer.parseInt(prop.getProperty("colCount")));
////				callableStatement.setInt(3, Integer.parseInt(prop.getProperty("tableCount")));
//				callableStatement.executeUpdate();
//				callableStatement.close();
//				conn.close();
//				endTime = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
//				System.out.println("end time is : " + endTime);
//				System.out.println(prop.getProperty("updateSP_name")+" SP executed successfully and check data in DB...!");
//			}
//		} catch (SQLException ex) {
//			ex.printStackTrace();
//		} finally {
//			try {
//				if (conn != null && !conn.isClosed()) {
//					conn.close();
//				}
//			} catch (SQLException ex) {
//				ex.printStackTrace();
//			}
//		}
//	}
//}

