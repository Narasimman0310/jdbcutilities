package com.indium.postgres;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

public class PostgresSpCall {
	static Properties prop=new Properties();
	private final String url = prop.getProperty("jdbc_url");
    private final String user = prop.getProperty("user_name");
    private final String password = prop.getProperty("password");
    public Connection connect() throws SQLException {
        return DriverManager.getConnection(url, user, password);
    }
    
    public void properCase(int sleepTime) throws InterruptedException, SQLException {
//        String result = s;
    	Connection conn = this.connect();
    	PreparedStatement properCase = conn.prepareStatement(prop.getProperty("call_sp_name"));
        try  {
//            properCase.registerOutParameter(1, Types.VARCHAR);
//        	properCase.setInt(1, 10);
        	for(int i=0;i<Integer.parseInt(prop.getProperty("loop_count"));i++)
        	{
            properCase.execute();
            Thread.sleep(Integer.parseInt(prop.getProperty("sleep_time_ms")));
        	}
        	System.out.println("**********Completed*******");
//            result = properCase.getString(1);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }finally {
            //Silently close off
            if (properCase != null) {
            	properCase.close();
            }

            if (conn != null) {
            	conn.close();
            }
        }
    }
    
    
    public static void main(String[] args) throws IOException, InterruptedException, SQLException {
    	System.out.println("start Time: "+java.time.LocalTime.now()); 
    	InputStream striimProp = new FileInputStream(args[0]);
    	PostgresSpCall.prop.load(striimProp);
    	for (String key: prop.stringPropertyNames()) {
			System.out.println(key + ": " + prop.getProperty(key));
		}
    	PostgresSpCall app = new PostgresSpCall();
        app.properCase(6000);
    }
}


