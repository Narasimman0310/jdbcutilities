package com.indium.postgres;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Properties;

import org.apache.commons.lang3.RandomStringUtils;

public class PostgresMultithreadInsert {
	//static Properties prop = new Properties();
	static Properties prop=new Properties();

	public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException, ParseException, SQLException {
		// TODO Auto-generated method stub
		InputStream striimProp = new FileInputStream(args[0]);
		prop.load(striimProp);
		for (String key: prop.stringPropertyNames()) {
			System.out.println(key + ": " + prop.getProperty(key));
		}
		System.out.println("start Time: "+java.time.LocalTime.now()); 
		
		//change here threadsize
		int insertThreadCount = Integer.parseInt(prop.getProperty("thread_count1"));
		System.out.println("Thread Count:"+insertThreadCount);
		
		
		Thread[] threadList=new Thread[insertThreadCount+1];
		
		for (int i = 1; i <= insertThreadCount; i++) 
            {
			System.out.println("iteration:"+i);
			String sp_table_name="table";
			//System.out.println(sp_table_name);
			
		//change here thread size
			if(insertThreadCount==Integer.parseInt(prop.getProperty("thread_count2")))
			{
				sp_table_name=sp_table_name+"_"+(i);
				System.out.println("Table iteration:"+sp_table_name);
				threadList[i] = new InsertThread(sp_table_name);
				//threadList[i]=new postgresInsert(sp_table_name);
				
				threadList[i].start();
			}
			//System.out.println("end Time: "+java.time.LocalTime.now());
			     
			else if(insertThreadCount==60){
				if(i%2==0)
				{
				sp_table_name=sp_table_name+(i);
				System.out.println("Table iteration:"+sp_table_name);
				}
				else {
					sp_table_name=sp_table_name+(i+1);
					System.out.println("Table iteration:"+sp_table_name);
				}
				threadList[i] = new InsertThread(sp_table_name);
				
				threadList[i].start();
			}
		//	System.out.println("end Time: "+java.time.LocalTime.now());
			     }
//			Thread object = new Thread(new InsertThread());
//			object.start();
//			Thread.sleep(5000);
		}
//		for (int i = 0; i < insertThreadCount; i++) {
//			threadList[i].join();
//		}
	}

class InsertThread extends Thread {
	
	public int colInt;
	public long longCol;
	public int smallIntCol;
	public String stringCol;
	public String textCol;
	public Date dateCol;
	public Time timeCol;
	public Timestamp timeStampCol;
	String table_name=null;
	
	SimpleDateFormat timeStampFormatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");  
	SimpleDateFormat timeFormatter=new SimpleDateFormat("HH:mm:ss");
	SimpleDateFormat dateFormatter=new SimpleDateFormat("dd-MM-yyyy");
	  
	java.util.Date date = new java.util.Date();
	public InsertThread(String tbl_name) throws ClassNotFoundException, ParseException, SQLException
	{
		this.table_name=tbl_name;
		System.out.println("test"+table_name);
		//System.out.println("end Time: "+java.time.LocalTime.now());
		//postgresInsert(table_name);
	}
//	    System.out.println(formatter.format(date));  
	@Override
	public void run()
	{
		//System.out.println("Maha");
				String url = PostgresMultithreadInsert.prop.getProperty("jdbc_url");
		        String user = PostgresMultithreadInsert.prop.getProperty("user");
		        String password = PostgresMultithreadInsert.prop.getProperty("password");
		        int totRow=Integer.parseInt(PostgresMultithreadInsert.prop.getProperty("rowCount"));
		        try {
					Class.forName("org.postgresql.Driver");
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		        Connection con = null;
				try {
					con = DriverManager.getConnection(url, user, password);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		        try {
					con.setAutoCommit(false);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		        PreparedStatement pst = null ;

		        try {
		        	
		        	String query=generateQuery(table_name);
		        	System.out.println("Executing the Query:"+query);
		        	pst = con.prepareStatement(query);
		            for(int rowCount=0;rowCount<totRow;rowCount++)
		            {
		            	int colInt1=generateInt();
		        		long longCol1=generateLong();
		        		int smallIntCol1=genrateSmallIntCol();
		        		String stringCol1=generateString();
		        		String textCol11=generateText();
		        		Date dateCol1=generateDate();
		        		Time timeCol1=genreateTimeCol();
		        		Timestamp timeStampCol1=generateTimestmap();
//		        		System.out.println(colInt1+","+longCol1+","+smallIntCol1+","+stringCol1+","+textCol11+","+dateCol1+","+timeCol1+","+timeStampCol1);
		        		int count=1;
		        		for(int i=0;i<12;i++)
		        		{
//		        			for(int j=0;j<8;j++)
//		        			{
		        				pst.setInt(count,colInt1);
		        				count++;
		        				pst.setLong(count,longCol1);
		        				count++;
		        				pst.setInt(count, smallIntCol1);
		        				count++;
		        				pst.setString(count, stringCol1);
		        				count++;
		        				pst.setString(count, textCol11);
		        				count++;
		        				pst.setDate(count, dateCol1);
		        				count++;
		        				pst.setTime(count, timeCol1);
		        				count++;
		        				pst.setTimestamp(count, timeStampCol1);
		        				count++;
//		        			}
		        		}
		        		
		        		pst.setInt(97, colInt1);
		        		pst.setLong(98, longCol1);
		        		pst.setInt(99, smallIntCol1);
		        		pst.executeUpdate();
	            		con.commit();
		        		
		        	//	pst.executeUpdate();
		         	//	con.commit();
		        		
		        		//pst.addBatch();
//		        		if(rowCount%1==0)
//		        		{
//		        			//pst.executeBatch();
//		        			//pst.clearBatch();
//		            		
//		        		}
//		        		System.out.println("Prepared Statement after bind variables set:\n\t" + pst.toString());
		            }
		           // pst.executeBatch();

		        } catch (SQLException ex) {
		        	ex.printStackTrace();
		        } catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
					try {
						if (pst != null)
							pst.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
					try {
						if (con != null)
							con.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
					System.out.println("end Time: "+java.time.LocalTime.now());
	}
	//System.out.println("end Time: "+java.time.LocalTime.now());
			
			public String generateQuery(String table_name) {
				StringBuilder sb=new StringBuilder();

				sb.append("INSERT INTO ").append(table_name).append(" (");
				for(int cno=1;cno<=99;cno++)
				{
					sb.append("col_").append(cno);
					if(cno==99)
						continue;
					else
						sb.append(",");
						
				}
				sb.append(")");
				sb.append(" VALUES(");
				
				for(int q=1;q<=99;q++)
				{
					sb.append("?");
					if(q==99)
						continue;
					else
						sb.append(",");
				}
				sb.append(")");
				return sb.toString();
			
			}
	public int generateInt()
	{
		colInt=getRandomNumberInRange(1, 99);
		return colInt;
	}
	
	public long generateLong()
	{
		longCol=getRandomNumberInRange(10000, 150000);
		return longCol;
	}
	
	public String generateString()
	{
		stringCol=RandomStringUtils.randomAlphabetic(10);
		return stringCol;
	}
	
	public String generateText()
	{
		textCol=RandomStringUtils.randomAlphabetic(20);
		return textCol;
	}
	
	public Date generateDate() throws ParseException
	{
		String tempDate=dateFormatter.format(date);
		java.util.Date parsedate = dateFormatter.parse(tempDate);
		dateCol = new java.sql.Date(parsedate.getTime());
		return dateCol;
	}
	public Timestamp generateTimestmap() throws ParseException
	{
		String tempTimeStamp=timeStampFormatter.format(date);
		timeStampCol=new Timestamp(timeStampFormatter.parse(tempTimeStamp).getTime());
		return timeStampCol;
	}
	
	public Time genreateTimeCol() throws ParseException
	{
		String tempTime=timeFormatter.format(date);
//		timeCol = (Time)timeFormatter.parse(tempTime);
		timeCol=new Time(timeFormatter.parse(tempTime).getTime());
		return timeCol;
	}
	public int genrateSmallIntCol()
	{
		smallIntCol=getRandomNumberInRange(100, 999);
		return smallIntCol;
	}
	
	private int getRandomNumberInRange(int min, int max) {

		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		return (int) (Math.random() * ((max - min) + 1)) + min;
	}}
		
	//public void postgresInsert(String table_name) throws ClassNotFoundException, ParseException, SQLException
