package com.indium.postgres;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.lang3.RandomStringUtils;

public class PostgresTest {
	public int colInt;
	public long longCol;
	public int smallIntCol;
	public String stringCol;
	public String textCol;
	public Date dateCol;
	public Time timeCol;
	public Timestamp timeStampCol;
	String table_name="table";
	
	SimpleDateFormat timeStampFormatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");  
	SimpleDateFormat timeFormatter=new SimpleDateFormat("HH:mm:ss");
	SimpleDateFormat dateFormatter=new SimpleDateFormat("dd-MM-yyyy");
	
	java.util.Date date = new java.util.Date();
	public String generateQuery(String tab_name) {
		StringBuilder sb=new StringBuilder();

		sb.append("WITH new_row1 AS\n(INSERT INTO ").append("table_"+1).append(" (");
		for(int counter=1 ; counter<4;counter++)
		{
		for(int cno=1;cno<=99;cno++)
		{
			sb.append("col_").append(cno);
			if(cno==99)
				continue;
			else
				sb.append(",");
				
		}
		sb.append(")");
		sb.append("\n");
		if(counter==2 || counter==3)
		{
		sb.append("VALUES( (select id_"+(counter-1)+" from new_row1),");
		}
		else {
			sb.append("VALUES(");
		}
		for(int q=1;q<=99;q++)
		{
			sb.append("?");
			if(q==99)
				continue;
			else
				sb.append(",");
		}
		if(counter==2 || counter==3)
		{
//			sb.deleteCharAt(sb.lastIndexOf(",?"));
			int index1=sb.lastIndexOf(",?");
			sb.replace(index1,",?".length()+index1,""); 
		}
		if(counter!=3)
		{	
		sb.append(")");
		sb.append("\n");
		sb.append("returning id_"+counter+" )");
		sb.append("\n");
		sb.append(",new_row2 as (insert into "+ "table_"+(counter+1)+" (");
		}
		}
		sb.append(")");
		int index=sb.lastIndexOf(",new_row2 as (");    
		sb.replace(index,",new_row2 as (".length()+index,""); 
		int index2=sb.lastIndexOf("new_row1"); 
		sb.replace(index2,"new_row1".length()+index2,"new_row2"); 
		return sb.toString();
	}
	
	public void insertion() {
		String url = "jdbc:postgresql://localhost:5432/postgres";
        String user = "postgres";
        String password = "postgres";
        int totRow=10000;
        try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        Connection con = null;
		try {
			con = DriverManager.getConnection(url, user, password);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        try {
			con.setAutoCommit(false);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        PreparedStatement pst = null ;

        try {
        	
        	String query=generateQuery(table_name);
        	System.out.println("Executing the Query:"+query);
        	pst = con.prepareStatement(query);
            for(int rowCount=0;rowCount<totRow;rowCount++)
            {
            	int colInt1=generateInt();
        		long longCol1=generateLong();
        		int smallIntCol1=genrateSmallIntCol();
        		String stringCol1=generateString();
        		String textCol11=generateText();
        		Date dateCol1=generateDate();
        		Time timeCol1=genreateTimeCol();
        		Timestamp timeStampCol1=generateTimestmap();
//        		System.out.println(colInt1+","+longCol1+","+smallIntCol1+","+stringCol1+","+textCol11+","+dateCol1+","+timeCol1+","+timeStampCol1);
        		int count=1;
				
        		for(int i=0;i<12;i++)
        		{
        				pst.setInt(count,colInt1);
    				    count++;
        				pst.setLong(count,longCol1);
        				count++;
        				pst.setInt(count, smallIntCol1);
        				count++;
        				pst.setString(count, stringCol1);
        				count++;
        				pst.setString(count, textCol11);
        				count++;
        				pst.setDate(count, dateCol1);
        				count++;
        				pst.setTime(count, timeCol1);
        				count++;
        				pst.setTimestamp(count, timeStampCol1);
        				count++;
        		}
        		pst.setInt(97, colInt1);
        		pst.setLong(98,longCol1);
				pst.setInt(99, smallIntCol1);
				
				count=100;
        		for(int i=0;i<12;i++)
        		{
        				pst.setLong(count,longCol1);
        				count++;
        				pst.setInt(count, smallIntCol1);
        				count++;
        				pst.setString(count, stringCol1);
        				count++;
        				pst.setString(count, textCol11);
        				count++;
        				pst.setDate(count, dateCol1);
        				count++;
        				pst.setTime(count, timeCol1);
        				count++;
        				pst.setTimestamp(count, timeStampCol1);
        				count++;
        				pst.setInt(count,colInt1);
    				    count++;
        		}
        		
        		
        		pst.setLong(196, longCol1);
        		pst.setInt(197, smallIntCol1);
        		
        		count=198;
        		
        		for(int i=0;i<12;i++)
        		{
        				pst.setLong(count,longCol1);
        				count++;
        				pst.setInt(count, smallIntCol1);
        				count++;
        				pst.setString(count, stringCol1);
        				count++;
        				pst.setString(count, textCol11);
        				count++;
        				pst.setDate(count, dateCol1);
        				count++;
        				pst.setTime(count, timeCol1);
        				count++;
        				pst.setTimestamp(count, timeStampCol1);
        				count++;
        				pst.setInt(count,colInt1);
    				    count++;
        		}
        		pst.setLong(294, longCol1);
        		pst.setInt(295, smallIntCol1);
//				
				
        		pst.executeUpdate();
        		con.commit();
        		
        	//	pst.executeUpdate();
         	//	con.commit();
        		
        		//pst.addBatch();
//        		if(rowCount%1==0)
//        		{
//        			//pst.executeBatch();
//        			//pst.clearBatch();
//            		
//        		}
//        		System.out.println("Prepared Statement after bind variables set:\n\t" + pst.toString());
            }
           // pst.executeBatch();

        } catch (SQLException ex) {
        	ex.printStackTrace();
        } catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			try {
				if (pst != null)
					pst.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (con != null)
					con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

	}
	
	public int generateInt()
	{
		colInt=getRandomNumberInRange(1, 99);
		return colInt;
	}
	
	public long generateLong()
	{
		longCol=getRandomNumberInRange(10000, 150000);
		return longCol;
	}
	
	public String generateString()
	{
		stringCol=RandomStringUtils.randomAlphabetic(10);
		return stringCol;
	}
	
	public String generateText()
	{
		textCol=RandomStringUtils.randomAlphabetic(20);
		return textCol;
	}
	
	public Date generateDate() throws ParseException
	{
		String tempDate=dateFormatter.format(date);
		java.util.Date parsedate = dateFormatter.parse(tempDate);
		dateCol = new java.sql.Date(parsedate.getTime());
		return dateCol;
	}
	public Timestamp generateTimestmap() throws ParseException
	{
		String tempTimeStamp=timeStampFormatter.format(date);
		timeStampCol=new Timestamp(timeStampFormatter.parse(tempTimeStamp).getTime());
		return timeStampCol;
	}
	
	public Time genreateTimeCol() throws ParseException
	{
		String tempTime=timeFormatter.format(date);
//		timeCol = (Time)timeFormatter.parse(tempTime);
		timeCol=new Time(timeFormatter.parse(tempTime).getTime());
		return timeCol;
	}
	public int genrateSmallIntCol()
	{
		smallIntCol=getRandomNumberInRange(100, 999);
		return smallIntCol;
	}
	
	private int getRandomNumberInRange(int min, int max) {

		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		return (int) (Math.random() * ((max - min) + 1)) + min;
	}
	public static void main(String[] args) {
		System.out.println("start Time: "+java.time.LocalTime.now()); 
		PostgresTest testObj =new PostgresTest();
//		System.out.println(testObj.generateQuery("table"));
		testObj.insertion();
		System.out.println("End Time: "+java.time.LocalTime.now()); 
	}
}
